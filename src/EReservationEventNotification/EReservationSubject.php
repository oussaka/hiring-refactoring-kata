<?php
/**
 * Created by PhpStorm.
 * User: oussaka
 * Date: 15/08/2021
 * Time: 23:50
 */

namespace MathildeGrise\Recrutement\KataRefacto\EReservationEventNotification;

use MathildeGrise\Recrutement\KataRefacto\Framework\Application_ServiceLocator;
use MathildeGrise\Recrutement\KataRefacto\Models\EReservation;

class EReservationSubject implements \SplSubject
{
    const INFO_LOG_LEVEL = 'INFO';

    public $reservationId;

    private $observers;

    public function __construct()
    {
        $this->observers = new \SplObjectStorage();
    }

    public function attach(\SplObserver $observer)
    {
        $this->observers->attach($observer);

        return $this;
    }

    public function detach(\SplObserver $observer)
    {
        $this->observers->detach($observer);

        return $this;
    }

    public function notify(): void
    {
        foreach ($this->observers as $observer) {
            $observer->update($this);
        }
    }

    /**
     * @throws \Exception
     */
    public function created(EReservation $EReservation)
    {
        Application_ServiceLocator::get('logger')->log('Notify Sales team of new ereservation', self::INFO_LOG_LEVEL);
        $this->reservationId = $EReservation->getId();
        $this->notify();

        return $this;
    }
}