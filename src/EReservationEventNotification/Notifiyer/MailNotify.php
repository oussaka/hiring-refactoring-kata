<?php
/**
 * Created by PhpStorm.
 * User: oussaka
 * Date: 15/08/2021
 * Time: 23:51
 */

namespace MathildeGrise\Recrutement\KataRefacto\EReservationEventNotification\Notifiyer;

use MathildeGrise\Recrutement\KataRefacto\Framework\Application_ServiceLocator;
use MathildeGrise\Recrutement\KataRefacto\Framework\ApplicationContext;

class MailNotify implements \SplObserver
{
    const INFO_LOG_LEVEL = 'info';

    public function update(\SplSubject $subject): void
    {
        $mailer = Application_ServiceLocator::get('mailer');
        try {
            $mailer->sendNewEReservation(ApplicationContext::getInstance()->getConfig()['sales_team_email'], $subject->reservationId);
        } catch (\Exception $e) {
            Application_ServiceLocator::get('logger')->log("Error Send new Ereservation email - " . $e->getMessage(), self::INFO_LOG_LEVEL);
            throw $e;
        }
    }
}