<?php

namespace MathildeGrise\Recrutement\KataRefacto;

use Exception;
use MathildeGrise\Recrutement\KataRefacto\EReservationEventNotification\EReservationSubject;
use MathildeGrise\Recrutement\KataRefacto\EReservationEventNotification\MailNotify;
use MathildeGrise\Recrutement\KataRefacto\EReservationEventNotification\Subject;
use MathildeGrise\Recrutement\KataRefacto\Framework\Application_ServiceLocator;
use MathildeGrise\Recrutement\KataRefacto\Framework\ApplicationContext;
use MathildeGrise\Recrutement\KataRefacto\Framework\Response;
use MathildeGrise\Recrutement\KataRefacto\Models\Customer;
use MathildeGrise\Recrutement\KataRefacto\Models\EReservation;
use MathildeGrise\Recrutement\KataRefacto\Models\Product;
use MathildeGrise\Recrutement\KataRefacto\Models\Store;

class CreateReservation
{
    /**
     * log level used for logging E-reservation steps
     */
    const INFO_LOG_LEVEL = 'INFO';

    /**
     * used data parameters
     */
    const PRODUCT_SKU_PARAM = "productsku";
    const CUSTOMER_ID_PARAM = "customerid";

    /**
     * list of all mandatory data
     */
    const MANDATORY_PARAMS = [
        self::PRODUCT_SKU_PARAM,
        self::CUSTOMER_ID_PARAM,
    ];

    /**
     * @var Product
     */
    protected $oProd;

    /**
     * @var Store
     */
    protected $store;

    /**
     * @var Customer
     */
    protected $user;

    /**
     * @var EReservation
     */
    protected $eReservation;

    /**
     * create e-reservation
     *
     * @param array $data
     * @return Response
     * @throws Exception
     */
    public function create(array $data)
    {
        $response = new Response();

        // add logs to log init process
        Application_ServiceLocator::get('logger')->log('init create E-reservation process with parameters: ' . json_encode($data), self::INFO_LOG_LEVEL);

        /* check and set all request params */
        Application_ServiceLocator::get('logger')->log('check parameters', self::INFO_LOG_LEVEL);
        // check if all parameters are passed
        if (!$this->checkData($data)) {
            return $response->setCode(403);
        }
        $this->store = ApplicationContext::getInstance()->getCurrentStore();
        // set product from SKU
        $this->oProd = Application_ServiceLocator::get('product.repository')->getProductFromSkuByStore($data[self::PRODUCT_SKU_PARAM], $this->store->getId());
        // set user from costumer id
        $this->user = Application_ServiceLocator::get('customer.repository')->getById($data[self::CUSTOMER_ID_PARAM]);

        /* create e-reservation */

        /* check there is stock for the product */
        try {
            /** @var checkStockService $checkStockService */
            $checkStockService = Application_ServiceLocator::get('stock.check_product_availability');
            $checkStockService->isAvailable($this->store, $this->oProd);
        } catch (Exception $e) {
            return new Response($e->getMessage(), $e->getCode());
        }

        // Persist new e-reservation in DB
        Application_ServiceLocator::get('logger')->log('Create new E-reservation', self::INFO_LOG_LEVEL);
        $ereservationRepository = Application_ServiceLocator::get('ereservation.repository');

        $id = $ereservationRepository->nextId();
        // create new E-reservation
        $this->eReservation = new EReservation(
            $id,
            $this->store->getId(),
            $this->oProd->getSKU(),
            $this->oProd->getPrice(),
            $this->user->getId()
        );
        $ereservationRepository->save($this->eReservation);

        // Notify Observers
        (new EReservationSubject())
            ->attach(new EReservationEventNotification\Notifiyer\MailNotify())
            ->created($this->eReservation)
        ;

        Application_ServiceLocator::get('logger')->log('E-reservation created with success' . json_encode($data), self::INFO_LOG_LEVEL);

        // format final success response
        $response->setCode(201);
        $response->setData(['id' => $this->eReservation->getId()]);

        return $response;
    }

    /**
     * check if all parameters are passed
     *
     * @param array $data
     * @return bool
     */
    private function checkData(array $data)
    {
        foreach (self::MANDATORY_PARAMS as $param) {
            if (!isset($data[$param])) {
                return false;
            }
        }
        return true;
    }
}
