<?php

namespace MathildeGrise\Recrutement\KataRefacto\Models;

class Product
{
    /**
     * @var string
     */
    private $sku;
    /**
     * @var int
     */
    private $price;

    /**
     * Product constructor.
     * @param string $sku
     * @param int $price
     */
    public function __construct(string $sku, int $price)
    {
        $this->sku = $sku;
        $this->price = $price;
    }

    /**
     * @return string
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * @return int
     */
    public function getPrice()
    {
        if ($this->price > 100000) {
            if (preg_match('/^WAT/', $this->sku)) { // A watch
                return $this->price * (1 + 0.15);
            } else {
                return $this->price * (1 + 0.10);
            }
        }

        return $this->price;
    }
}