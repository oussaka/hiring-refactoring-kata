<?php
/**
 * Created by PhpStorm.
 * User: oussaka
 * Date: 15/08/2021
 * Time: 22:11
 */

namespace MathildeGrise\Recrutement\KataRefacto;

use Exception;
use MathildeGrise\Recrutement\KataRefacto\Framework\Application_ServiceLocator;
use MathildeGrise\Recrutement\KataRefacto\Framework\Response;
use MathildeGrise\Recrutement\KataRefacto\Models\Product;
use MathildeGrise\Recrutement\KataRefacto\Models\Store;

class checkStockService
{
    const INFO_LOG_LEVEL = 'INFO';

    public function __construct()
    {
        // Application_ServiceLocator::get('stock.product_availability');
    }

    /**
     * @param Store   $store
     * @param Product $product
     *
     * @return array
     * @throws Exception
     */
    public function isAvailable(Store $store, Product $product)
    {
        $response = new Response();

        Application_ServiceLocator::get('logger')->log('Determine if there is stock for the product on the store', self::INFO_LOG_LEVEL);
        $stock = Application_ServiceLocator::get('stock.product_availability');
        try {
            $stock = $stock->getStockLevelByStore($store->getId(), $product);
        } catch (Exception $e) {
            // log the error status
            Application_ServiceLocator::get('logger')->log("Error StockByStore - " . $e->getMessage(), self::INFO_LOG_LEVEL);
            throw new Exception('Service does not respond', 500);
        }
        // Check availability
        if (!$stock['Available']) {
            throw new Exception('Out of stock', 400);
        }

        return ['Available' => true];
    }
}